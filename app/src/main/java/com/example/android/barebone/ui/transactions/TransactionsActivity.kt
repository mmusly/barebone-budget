package com.example.android.barebone.ui.transactions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.barebone.R
import com.example.android.barebone.data.model.CategoryItemModel
import com.example.android.barebone.databinding.ActivityTransactionsBinding
import com.example.android.barebone.ui.assigning.AssigningActivity
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.extensions.onChanged
import com.example.android.barebone.ui.home.homefragments.fragmenta.TransactionsAdapter
import com.google.gson.Gson
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_transactions.*
import timber.log.Timber
import javax.inject.Inject

class TransactionsActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: TransactionsViewModel
    private lateinit var binding: ActivityTransactionsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_transactions)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TransactionsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.transMessage.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.d("Success")
                    setupTransactionList(binding.tranRecycler)
                    saveCategoriesToSharedPref()
                }
                is Result.Error -> {
                    Timber.d("Error")
                }
            }
        }
    }

    private fun setupTransactionList (tranList: RecyclerView) {

        val transactionsAdapter = TransactionsAdapter { data ->
            Timber.d("Item Clicked: $data")
            //viewModel.onItemClicked(data)

            Intent(this, AssigningActivity::class.java).apply {
                putExtra("item", data)
                startActivityForResult(this, 5)
            }
        }

        tranList.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@TransactionsActivity)
            adapter = transactionsAdapter
        }

        viewModel.data.observe(this, Observer { result ->
            transactionsAdapter.submitList(result)
            myTextView.text = "יש לך ${result.size} פעולות חדשות במערכת "
            load_animation.visibility = View.GONE
            constraintLayout.visibility = View.VISIBLE
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                val tran = data!!.getParcelableExtra<TranItemModel>("clickedTran")
                viewModel.onItemClicked(tran)
            }
        }
    }

    private fun saveCategoriesToSharedPref() {

        var categoriesList: List<CategoryItemModel>
        categoriesList = viewModel.categData.value!!.toList()

        val gson = Gson()
        val userInfoListJsonString = gson.toJson(categoriesList)

        val sharedPref = this.getSharedPreferences("transaction",Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("tranItem", userInfoListJsonString)
            commit()
        }
    }
}
