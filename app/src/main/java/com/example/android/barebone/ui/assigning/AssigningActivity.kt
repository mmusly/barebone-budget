package com.example.android.barebone.ui.assigning

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.barebone.R
import com.example.android.barebone.databinding.ActivityAssigningBinding
import com.example.android.barebone.ui.transactions.TranItemModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_assigning.*
import timber.log.Timber
import javax.inject.Inject

class AssigningActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: AssigningViewModel
    private lateinit var binding: ActivityAssigningBinding

    private  var mTranItem: TranItemModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_assigning)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AssigningViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        mTranItem = intent.getParcelableExtra("item")
        transactionDesc.text = mTranItem?.desc
        transactionDate.text = mTranItem?.date
        transactionAmount.text = mTranItem?.amount
        viewModel.getSharedData(this)
        setupCatgoriesList(binding.categoriesRecycler)

    }

    private fun setupCatgoriesList (categList: RecyclerView) {

        val assigningAdapter = AssigningAdapter { data ->
            Timber.d("Item Clicked: $data")
            viewModel.onItemClicked(data, mTranItem, "", this)

        }

        categList.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@AssigningActivity)
            adapter = assigningAdapter
        }

        viewModel.data.observe(this, Observer { result ->
            assigningAdapter.submitList(result)
        })
    }


}
