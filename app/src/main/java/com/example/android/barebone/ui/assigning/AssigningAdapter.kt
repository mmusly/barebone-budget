package com.example.android.barebone.ui.assigning

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.example.android.barebone.R
import com.example.android.barebone.data.model.CategoryItemModel
import com.example.android.barebone.databinding.ListItemCategoryBinding
import com.example.android.barebone.ui.common.DataBoundListAdapter


class AssigningAdapter(
    private val itemClickCallback: ((CategoryItemModel) -> Unit)?
) : DataBoundListAdapter<CategoryItemModel, ListItemCategoryBinding>(
    diffCallback = object : DiffUtil.ItemCallback<CategoryItemModel>() {
        override fun areItemsTheSame(oldItem: CategoryItemModel, newItem: CategoryItemModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CategoryItemModel, newItem: CategoryItemModel): Boolean {
            return oldItem == newItem
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ListItemCategoryBinding {
        val binding = DataBindingUtil.inflate<ListItemCategoryBinding>(
            LayoutInflater.from(parent.context), R.layout.list_item_category,
            parent, false
        )

        binding.categoryItem.setOnClickListener {
            binding.data?.let {
                itemClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ListItemCategoryBinding, item: CategoryItemModel) {
        binding.data = item
    }
}