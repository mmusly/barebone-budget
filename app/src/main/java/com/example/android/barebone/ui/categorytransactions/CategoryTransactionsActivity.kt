package com.example.android.barebone.ui.categorytransactions

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.android.barebone.R
import com.example.android.barebone.databinding.ActivityCategoryTransactionsBinding
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.extensions.onChanged
import com.example.android.barebone.ui.home.homefragments.SummaryItemModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_category_transactions.*
import timber.log.Timber
import javax.inject.Inject

class CategoryTransactionsActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: CategoryTransactionsViewModel
    private lateinit var binding: ActivityCategoryTransactionsBinding
    private  var mSummaryItem: SummaryItemModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category_transactions)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CategoryTransactionsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        mSummaryItem = intent.getParcelableExtra("item")
        detailsSubTitle.text = mSummaryItem?.categoryname
        limitAmount.text = mSummaryItem?.limitFormatted
        currAmount.text = mSummaryItem?.usageFormatted
        freeAmount.text = mSummaryItem?.freeFormatted

        viewModel.transMessage.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.d("Success")
//                    setupTransactionList(binding.tranRecycler)
//                    saveCategoriesToSharedPref()
                }
                is Result.Error -> {
                    Timber.d("Error")
                }
            }
        }
    }
}
