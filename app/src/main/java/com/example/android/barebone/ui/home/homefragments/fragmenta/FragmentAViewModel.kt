package com.example.android.barebone.ui.home.homefragments.fragmenta

import android.content.SharedPreferences
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.barebone.api.WebServiceApi
import com.example.android.barebone.data.model.CategoryItemModel
import com.example.android.barebone.data.model.ResponseCategories
import com.example.android.barebone.data.model.ResponseTransactions
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.extensions.LiveEvent
import com.example.android.barebone.ui.transactions.TranItemModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import timber.log.Timber
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FragmentAViewModel @Inject constructor(
    sharedPreferences: SharedPreferences,private val api: WebServiceApi
) : ViewModel() {

    private val _featureXEvent = LiveEvent<Unit>()
    private val _featureYEvent = LiveEvent<Unit>()
    private val _featureZEvent = LiveEvent<Unit>()
    val featureXEvent: LiveData<Unit> = _featureXEvent
    val featureYEvent: LiveData<Unit> = _featureYEvent
    val featureZEvent: LiveData<Unit> = _featureZEvent

    private val compositeDisposable = CompositeDisposable()
    val isOperationInProgress = ObservableField(false)

    val transMessage = ObservableField<Result<ArrayList<ResponseTransactions>>>()
    val categoriesMessage = ObservableField<Result<ArrayList<ResponseCategories>>>()

    private val _data = MutableLiveData<List<TranItemModel>>()
    val data: LiveData<List<TranItemModel>> = _data

    private val _categData = MutableLiveData<List<CategoryItemModel>>()
    val categData: LiveData<List<CategoryItemModel>> = _categData

//    private val _preferredData = MutableLiveData<List<PreferItemModel>>()
//    val preferredData: LiveData<List<PreferItemModel>> = _preferredData

    private val dataObject = mutableListOf<TranItemModel>()
    private val categDataObject = mutableListOf<CategoryItemModel>()

    init {
        Timber.i("Got injected shared preferences: %s", sharedPreferences)
        categoriesRequest(111, 202002)
    }

    fun categoriesRequest(custid: Int, period: Int) {

        compositeDisposable.add(
            api.getCategories(custid, period)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    categoriesMessage.set(Result.Success(it))
                    prepareCategoriesDataSet(it)
                    _categData.value = categDataObject.toList()
                    transactionsRequest(custid.toString())
                }, { error ->
                    Timber.e(error)
                    categoriesMessage.set(Result.Error(error))
                })
        )
    }

    fun transactionsRequest(custid: String) {
        Timber.i("Sending web request with name: $custid")

        compositeDisposable.add(
            api.getTransactions(custid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    transMessage.set(Result.Success(it))
                    prepareTransactionsDataSet(it)
                }, { error ->
                    Timber.e(error)
                    transMessage.set(Result.Error(error))
                })
        )

    }

    private fun prepareCategoriesDataSet(list: ArrayList<ResponseCategories>) {
        categDataObject.clear()
        for (i in 0 until list.size) {
            categDataObject.add(
                CategoryItemModel(
                    list[i].categoryid!!,
                    list[i].categoryname!!,
                    list[i].limitamount!!.toString(),
                    list[i].currentamount!!.toString()
                )
            )
        }
    }

    private fun prepareTransactionsDataSet (list: ArrayList<ResponseTransactions>) {
        dataObject.clear()
        for (i in 0 until list.size) {

            val inputDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
            val outputDate = SimpleDateFormat("dd.MM.YYYY", Locale.US)

            val d = inputDate.parse(list[i].tIMESTAMP.toString())
            val dateFormatted = outputDate.format(d)

            dataObject.add(TranItemModel(
                list[i].id!!,
                list[i].desc.toString(),
                dateFormatted,
                NumberFormat.getNumberInstance(Locale.US).format(list[i].amount!!).toString(),
                list[i].creditcardnumber!!))
        }
        _data.value = dataObject.toList()
        isOperationInProgress.set(false)
    }


//    fun getPrefrencesBusinessData(context: Context) {
//        val prefData = context.getSharedPreferences("businessPref", Context.MODE_PRIVATE) ?: return
//        val prefObject = prefData.getString("preferItem", "")
//
//        val gson = Gson()
//        var preferInfo: Array<PreferItemModel>? = gson.fromJson(prefObject, Array<PreferItemModel>::class.java)
//
//        _preferredData.value = preferInfo?.toList()
//    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun onItemClicked(item: TranItemModel) {
        dataObject.remove(item)
        _data.value = dataObject.toList()
    }

    fun openFeatureXClicked() {
        _featureXEvent.value = Unit
    }

    fun openFeatureYClicked() {
        _featureYEvent.value = Unit
    }

    fun openFeatureZClicked() {
        _featureZEvent.value = Unit
    }
}
