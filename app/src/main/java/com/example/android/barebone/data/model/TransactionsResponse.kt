package com.example.android.barebone.data.model

import com.google.gson.annotations.SerializedName

data class TransactionsResponse(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("notes")
	val notes: Any? = null,

	@field:SerializedName("TIMESTAMP")
	val tIMESTAMP: String? = null,

	@field:SerializedName("customerid")
	val customerid: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("categoryname")
	val categoryname: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("origid")
	val origid: String? = null,

	@field:SerializedName("categoryid")
	val categoryid: Int? = null,

	@field:SerializedName("creditcardnumber")
	val creditcardnumber: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)