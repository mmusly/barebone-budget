package com.example.android.barebone.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
