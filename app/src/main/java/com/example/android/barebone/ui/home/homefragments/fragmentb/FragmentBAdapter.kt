package com.example.android.barebone.ui.home.homefragments.fragmentb

import android.content.res.Resources
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.example.android.barebone.R
import com.example.android.barebone.databinding.ListItemSummaryBinding
import com.example.android.barebone.ui.common.DataBoundListAdapter
import com.example.android.barebone.ui.home.homefragments.SummaryItemModel

class FragmentBAdapter(
    private val itemClickCallback: ((SummaryItemModel) -> Unit)?
) : DataBoundListAdapter<SummaryItemModel, ListItemSummaryBinding>(
    diffCallback = object : DiffUtil.ItemCallback<SummaryItemModel>() {
        override fun areItemsTheSame(oldItem: SummaryItemModel, newItem: SummaryItemModel): Boolean {
            return oldItem.categoryid == newItem.categoryid
        }

        override fun areContentsTheSame(oldItem: SummaryItemModel, newItem: SummaryItemModel): Boolean {
            return oldItem == newItem
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ListItemSummaryBinding {
        val binding = DataBindingUtil.inflate<ListItemSummaryBinding>(
            LayoutInflater.from(parent.context), R.layout.list_item_summary,
            parent, false
        )

        binding.summaryItem.setOnClickListener {
            binding.data?.let {
                itemClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ListItemSummaryBinding, item: SummaryItemModel) {
        binding.data = item

        binding.limitTitle.setTextColor(binding.limitTitle.context.resources.getColor(R.color.colorTextLight))
        binding.limitAmount.setTextColor(binding.limitAmount.context.resources.getColor(R.color.colorTextLight))


        if (item.limit == 0.0) {
            binding.freeTitle.setTextColor(binding.freeTitle.context.resources.getColor(R.color.colorNoBudgetText))
            binding.freeAmount.setTextColor(binding.freeAmount.context.resources.getColor(R.color.colorNoBudgetText))
            binding.freeTitle.text = binding.freeTitle.context.getString(R.string.no_budget)
        } else {
            if (item.usage == 0.0) {
                binding.freeTitle.setTextColor(binding.freeTitle.context.resources.getColor(R.color.colorNoUsageText))
                binding.freeAmount.setTextColor(binding.freeAmount.context.resources.getColor(R.color.colorNoUsageText))
                binding.freeTitle.text = binding.freeTitle.context.getString(R.string.full_free_amount)
            } else if (item.usage > 0.0 && item.usage < item.limit) {
                binding.freeTitle.setTextColor(binding.freeTitle.context.resources.getColor(R.color.colorOKBudgetText))
                binding.freeAmount.setTextColor(binding.freeAmount.context.resources.getColor(R.color.colorOKBudgetText))
                binding.freeTitle.text = binding.freeTitle.context.getString(R.string.current_free_amount)
            } else if (item.usage == item.limit) {
                binding.freeTitle.setTextColor(binding.freeTitle.context.resources.getColor(R.color.colorFullBudgetText))
                binding.freeAmount.setTextColor(binding.freeAmount.context.resources.getColor(R.color.colorFullBudgetText))
                binding.freeTitle.text = binding.freeTitle.context.getString(R.string.no_free_amount)
            } else if (item.usage > item.limit) {
                binding.freeTitle.setTextColor(binding.freeTitle.context.resources.getColor(R.color.colorExceededBudgetText))
                binding.freeAmount.setTextColor(binding.freeAmount.context.resources.getColor(R.color.colorExceededBudgetText))
                binding.freeTitle.text = binding.freeTitle.context.getString(R.string.exceeded_budget)
            }
        }
    }
}