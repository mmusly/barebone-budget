package com.example.android.barebone.ui.transactions

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TranItemModel (
    val id: Int,
    val desc: String,
    val date: String,
    val amount: String,
    val paymentMethod: String
): Parcelable