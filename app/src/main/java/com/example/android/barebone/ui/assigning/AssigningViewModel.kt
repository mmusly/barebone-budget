package com.example.android.barebone.ui.assigning

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.barebone.api.WebServiceApi
import com.example.android.barebone.data.model.CategoryItemModel
import com.example.android.barebone.data.model.UpdateRowResponse
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.transactions.TranItemModel
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class AssigningViewModel @Inject constructor(private val api: WebServiceApi) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val isOperationInProgress = ObservableField(false)
    val serviceResposnse = ObservableField(0)
    private val _data = MutableLiveData<List<CategoryItemModel>>()
    val data: LiveData<List<CategoryItemModel>> = _data
    val updTranResponse = ObservableField<Result<UpdateRowResponse>>()

    private val sampleData = mutableListOf<CategoryItemModel>()

    init {}

    fun onItemClicked(categ: CategoryItemModel, tran: TranItemModel?, note: String, con: AssigningActivity) {
        updateTranCategory(categ, tran, note)

        var i: Intent = Intent()
        i.putExtra("clickedTran", tran)
        con.setResult(Activity.RESULT_OK, i)
        con.finish()
    }

    fun updateTranCategory(categ: CategoryItemModel, tran: TranItemModel?, note: String) {

        val bodyObj = UpdateTranCategRequest(tran?.id, categ?.id, categ?.name, note)

        compositeDisposable.add(
            api.updateTranCategory(bodyObj)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    updTranResponse.set(Result.Success(it))
                    serviceResposnse.set(1)
                    _data.value = sampleData.toList()
                }, { error ->
                    Timber.e(error)
                    serviceResposnse.set(2)
                    updTranResponse.set(Result.Error(error))
                })
        )
    }

    fun getSharedData(context: Context) {

        val sharedPref = context.getSharedPreferences("transaction",Context.MODE_PRIVATE) ?: return
        val highScore = sharedPref.getString("tranItem", "")

        val gson = Gson()
        var userInfo: Array<CategoryItemModel>? = gson.fromJson(highScore, Array<CategoryItemModel>::class.java)

        _data.value = userInfo?.toList()

    }

}