package com.example.android.barebone.api

import com.example.android.barebone.data.model.*
import com.example.android.barebone.ui.assigning.UpdateTranCategRequest
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Web service API.
 * TODO: Replace with your API
 */
interface WebServiceApi {
    @GET("/say/hello")
    fun hello(@Query("name") name: String):
            Single<ResponseMessage>

    @GET("getuncategorizedtransactions")
    fun getTransactions(
        @Query("custid") custid: String):
            Single<ArrayList<ResponseTransactions>>

    @GET("getcategorytransactions")
    fun getCategoryTransaction(
        @Query("custid") custid: String,
        @Query("categid") categid: Int):
            Single<ArrayList<ResponseTransactions>>

    @GET("getcategories")
    fun getCategories(
        @Query("custid") custid: Int,
        @Query("period") period: Int
    ): Single<ArrayList<ResponseCategories>>

    @POST("updatetrancategory")
    fun updateTranCategory(
        @Body body: UpdateTranCategRequest
    ): Single<UpdateRowResponse>

    @GET("getsummary")
    fun getSummary(
        @Query("custid") custid: Int,
        @Query("period") period: Int):
            Single<ArrayList<SummaryResponse>>

}
