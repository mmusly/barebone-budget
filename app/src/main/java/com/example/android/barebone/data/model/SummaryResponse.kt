package com.example.android.barebone.data.model

import com.google.gson.annotations.SerializedName

data class SummaryResponse(

	@field:SerializedName("period")
	val period: Int? = null,

	@field:SerializedName("usage")
	val usage: Double? = null,

	@field:SerializedName("custid")
	val custid: Int? = null,

	@field:SerializedName("limit")
	val limit: Int? = null,

	@field:SerializedName("categoryname")
	val categoryname: String? = null,

	@field:SerializedName("free")
	val free: Int? = null,

	@field:SerializedName("categoryid")
	val categoryid: Int? = null
)