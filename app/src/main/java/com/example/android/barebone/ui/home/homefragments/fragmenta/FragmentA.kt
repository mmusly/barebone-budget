package com.example.android.barebone.ui.home.homefragments.fragmenta

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.barebone.R
import com.example.android.barebone.data.model.CategoryItemModel
import com.example.android.barebone.databinding.FragmentABinding
import com.example.android.barebone.di.Injectable
import com.example.android.barebone.ui.assigning.AssigningActivity
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.extensions.onChanged
import com.example.android.barebone.ui.featurex.FeatureXActivity
import com.example.android.barebone.ui.featurey.FeatureYActivity
import com.example.android.barebone.ui.featurez.FeatureZActivity
import com.example.android.barebone.ui.transactions.TranItemModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_transactions.load_animation
import kotlinx.android.synthetic.main.activity_transactions.myTextView
import kotlinx.android.synthetic.main.fragment_a.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Demo fragment for tab content.
 *
 * This fragment is different than [FragmentB] and [FragmentC] in following ways:
 * - This has it's own view model [FragmentAViewModel] which is provided via injected factory.
 * - This loads a custom layout to test other activities.
 *
 * TODO: Move the fragment to it's own feature package.
 */
class FragmentA : Fragment(), Injectable {
    companion object {
        fun createInstance(): FragmentA {
            return FragmentA()
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentABinding
    lateinit var viewModel: FragmentAViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = activity?.run {
            ViewModelProviders.of(this, viewModelFactory).get(FragmentAViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        Timber.d("Got injected fragment's own viewmodel instance: %s.", viewModel)

        viewModel.transMessage.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.d("Success")
                    setupTransactionList(binding.tranRecycler)
                    saveCategoriesToSharedPref()
                }
                is Result.Error -> {
                    Timber.d("Error")
                }
            }
        }

        // Inflate the layout for this fragment using data binding and set the view model
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_a, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    private fun setupTransactionList (tranList: RecyclerView) {

        val transactionsAdapter = TransactionsAdapter { data ->
            Timber.d("Item Clicked: $data")
            //viewModel.onItemClicked(data)

            Intent(activity, AssigningActivity::class.java).apply {
                putExtra("item", data)
                startActivityForResult(this, 5)
            }
        }

        tranList.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(activity)
            adapter = transactionsAdapter
        }

        viewModel.data.observe(this, Observer { result ->
            transactionsAdapter.submitList(result)
            myTextView.text = "יש לך ${result.size} תנועות חדשות במערכת "
            load_animation.visibility = View.GONE
            subTitle.visibility = View.VISIBLE
            transactionsMainTitle.visibility = View.VISIBLE
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavigationEvents(viewModel)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                val tran = data!!.getParcelableExtra<TranItemModel>("clickedTran")
                viewModel.onItemClicked(tran)
            }
        }
    }

    private fun saveCategoriesToSharedPref() {

        var categoriesList: List<CategoryItemModel>
        categoriesList = viewModel.categData.value!!.toList()

        val gson = Gson()
        val userInfoListJsonString = gson.toJson(categoriesList)

        val sharedPref = activity?.getSharedPreferences("transaction", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("tranItem", userInfoListJsonString)
            commit()
        }
    }

    /**
     * TODO: This is an example of how LiveData can be used to navigate. Update accordingly.
     */
    private fun observeNavigationEvents(viewModel: FragmentAViewModel) {
        viewModel.featureXEvent.observe(this, Observer {
            Timber.i("Launching feature X activity.")
            startActivity(Intent(activity, FeatureXActivity::class.java))
        })

        viewModel.featureYEvent.observe(this, Observer {
            Timber.i("Launching feature Y activity.")
            startActivity(Intent(activity, FeatureYActivity::class.java))
        })

        viewModel.featureZEvent.observe(this, Observer {
            Timber.i("Launching feature Z activity.")
            startActivity(Intent(activity, FeatureZActivity::class.java))
        })
    }
}
