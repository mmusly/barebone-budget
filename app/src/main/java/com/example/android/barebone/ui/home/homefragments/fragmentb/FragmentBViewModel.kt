package com.example.android.barebone.ui.home.homefragments.fragmentb

import android.content.SharedPreferences
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.barebone.api.WebServiceApi
import com.example.android.barebone.data.model.SummaryResponse
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.home.homefragments.SummaryItemModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class FragmentBViewModel @Inject constructor(
    sharedPreferences: SharedPreferences, private val api: WebServiceApi
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val isOperationInProgress = ObservableField(false)

    val summaryMessage = ObservableField<Result<ArrayList<SummaryResponse>>>()
    private val _summaryData = MutableLiveData<List<SummaryItemModel>>()
    val summaryData: LiveData<List<SummaryItemModel>> = _summaryData
    private val summaryDataObject = mutableListOf<SummaryItemModel>()

    init {
        summaryRequest(111, 202002)
    }

    fun summaryRequest(custid: Int, period: Int) {
        compositeDisposable.add(
            api.getSummary(custid, period)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    summaryMessage.set(Result.Success(it))
                    prepareSummaryDataSet(it)
                }, { error ->
                    Timber.e(error)
                    summaryMessage.set(Result.Error(error))
                })
        )
    }

    private fun prepareSummaryDataSet(list: ArrayList<SummaryResponse>) {
        summaryDataObject.clear()
        for (i in 0 until list.size) {
            summaryDataObject.add(
                SummaryItemModel(
                    list[i].categoryid!!,
                    list[i].categoryname!!,
                    list[i].limit!!.toDouble(),
                    NumberFormat.getNumberInstance(Locale.US).format(list[i].limit).toString(),
                    "הקצאה חודשית",
                    list[i].usage!!.toDouble(),
                    NumberFormat.getNumberInstance(Locale.US).format(list[i].usage).toString(),
                    list[i].free!!.toDouble(),
                    NumberFormat.getNumberInstance(Locale.US).format(list[i].free).toString(),
                    "יתרה לשימוש"
                )
            )
        }

        _summaryData.value = summaryDataObject.toList()
        isOperationInProgress.set(false)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}