package com.example.android.barebone.ui.home.homefragments

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SummaryItemModel (
    val categoryid: Int,
    val categoryname: String,
    val limit: Double,
    val limitFormatted: String,
    val limitTitle: String,
    val usage: Double,
    val usageFormatted: String,
    val free: Double,
    val freeFormatted: String,
    val freeTitle: String
): Parcelable
