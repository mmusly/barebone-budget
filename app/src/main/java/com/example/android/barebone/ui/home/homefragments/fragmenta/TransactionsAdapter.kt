package com.example.android.barebone.ui.home.homefragments.fragmenta

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.example.android.barebone.R
import com.example.android.barebone.databinding.ListItemTransactionBinding
import com.example.android.barebone.ui.common.DataBoundListAdapter
import com.example.android.barebone.ui.transactions.TranItemModel

class TransactionsAdapter(
    private val itemClickCallback: ((TranItemModel) -> Unit)?
) : DataBoundListAdapter<TranItemModel, ListItemTransactionBinding>(
    diffCallback = object : DiffUtil.ItemCallback<TranItemModel>() {
        override fun areItemsTheSame(oldItem: TranItemModel, newItem: TranItemModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TranItemModel, newItem: TranItemModel): Boolean {
            return oldItem == newItem
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ListItemTransactionBinding {
        val binding = DataBindingUtil.inflate<ListItemTransactionBinding>(
            LayoutInflater.from(parent.context), R.layout.list_item_transaction,
            parent, false
        )

        binding.transactionItem.setOnClickListener {
            binding.data?.let {
                itemClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ListItemTransactionBinding, item: TranItemModel) {
        binding.data = item
    }
}