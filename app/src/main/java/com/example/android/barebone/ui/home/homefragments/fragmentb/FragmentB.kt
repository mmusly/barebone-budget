package com.example.android.barebone.ui.home.homefragments.fragmentb

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.barebone.R
import com.example.android.barebone.databinding.FragmentCategoriesBinding
import com.example.android.barebone.di.Injectable
import com.example.android.barebone.ui.categorytransactions.CategoryTransactionsActivity
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.extensions.onChanged
import kotlinx.android.synthetic.main.activity_transactions.load_animation
import kotlinx.android.synthetic.main.fragment_categories.*
import timber.log.Timber
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

/**
 * Demo fragment for tab content.
 *
 * TODO: Move the fragment to it's own feature package.
 */
class FragmentB : Fragment(), Injectable {
    companion object {
        fun createInstance(): FragmentB {
            return FragmentB()
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentCategoriesBinding
    lateinit var viewModel: FragmentBViewModel

    override fun onResume() {
        super.onResume()
        //load_animation.visibility = View.VISIBLE
        viewModel.summaryRequest(111,202002)
        setupSummaryList(binding.categoriesRecycler)
        //load_animation.visibility = View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = activity?.run {
            ViewModelProviders.of(this, viewModelFactory).get(FragmentBViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        Timber.d("Got injected parent's viewmodel instance: %s.", viewModel)

        viewModel.summaryMessage.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.d("Success")
                    setupSummaryList(binding.categoriesRecycler)
                }
                is Result.Error -> {
                    Timber.d("Error")
                }
            }
        }

        // Inflate the layout for this fragment using data binding and set the view model
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false)
        binding.viewModel = viewModel
        return binding.root

    }

    private fun setupSummaryList (summaryList: RecyclerView) {

        val summaryAdapter =
            FragmentBAdapter { data ->
                Timber.d("Item Clicked: $data")
                //viewModel.onItemClicked(data)

                Intent(activity, CategoryTransactionsActivity::class.java).apply {
                    putExtra("item", data)
                    startActivityForResult(this, 5)
                }
            }

        summaryList.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(activity)
            adapter = summaryAdapter
        }

        viewModel.summaryData.observe(this, Observer { result ->

            var limitSummary=0.0
            var usageSummary=0.0
            var freeSummary=0.0

            for (i in 0 until result.size) {
                limitSummary += result[i].limit
                usageSummary += result[i].usage
                freeSummary += result[i].free
            }

            sumLimit.text = NumberFormat.getNumberInstance(Locale.US).format(limitSummary).toString()
            sumCurrent.text = NumberFormat.getNumberInstance(Locale.US).format(usageSummary).toString()
            sumFree.text = NumberFormat.getNumberInstance(Locale.US).format(freeSummary).toString()

            summaryAdapter.submitList(result)
            load_animation.visibility = View.GONE
            limitTitleSum.visibility = View.VISIBLE
            currTitleSum.visibility = View.VISIBLE
            freeTitleSum.visibility = View.VISIBLE
            summaryMainTitle.visibility = View.VISIBLE
        })
    }
}
