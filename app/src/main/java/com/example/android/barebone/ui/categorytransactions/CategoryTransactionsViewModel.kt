package com.example.android.barebone.ui.categorytransactions

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.barebone.api.WebServiceApi
import com.example.android.barebone.data.model.ResponseTransactions
import com.example.android.barebone.ui.common.Result
import com.example.android.barebone.ui.transactions.TranItemModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class CategoryTransactionsViewModel @Inject constructor(private val api: WebServiceApi) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val isOperationInProgress = ObservableField(false)

    val transMessage = ObservableField<Result<ArrayList<ResponseTransactions>>>()

    private val _data = MutableLiveData<List<TranItemModel>>()
    val data: LiveData<List<TranItemModel>> = _data

    private val dataObject = mutableListOf<TranItemModel>()

    init {
        transactionsRequest("111", "202002", 1)
    }

    fun transactionsRequest(custid: String, period: String, categid: Int) {
        Timber.i("Sending web request with name: $custid")

        compositeDisposable.add(
            api.getCategoryTransaction(custid, categid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    transMessage.set(Result.Success(it))
                    prepareTransactionsDataSet(it)
                }, { error ->
                    Timber.e(error)
                    transMessage.set(Result.Error(error))
                })
        )
    }

    private fun prepareTransactionsDataSet (list: ArrayList<ResponseTransactions>) {
        dataObject.clear()
        for (i in 0 until list.size) {

            val inputDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
            val outputDate = SimpleDateFormat("dd MMM YYYY", Locale.US)

            val d = inputDate.parse(list[i].tIMESTAMP.toString())
            val dateFormatted = outputDate.format(d)

            dataObject.add(TranItemModel(list[i].id!!, list[i].desc.toString(), dateFormatted, list[i].amount.toString(),list[i].creditcardnumber!!))
        }
        _data.value = dataObject.toList()
        isOperationInProgress.set(false)
    }
}