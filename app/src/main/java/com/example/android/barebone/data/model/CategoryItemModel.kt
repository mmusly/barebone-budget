package com.example.android.barebone.data.model

data class CategoryItemModel (val id: Int, val name: String, val limit: String, val current: String){}